#include<stdio.h>

#define FixedCost 500
#define VariableCost 3

int NoOfViewers(int price);
int Income(int price);
int Cost(int price);
int Profit(int price);

int	NoOfViewers(int price){
	return 120-((price-15)/5)*20;
}

int Income(int price){
	return NoOfViewers(price)*price;
}

int Cost(int price){
	return NoOfViewers(price)*VariableCost+FixedCost;
}

int Profit(int price){
	return Income(price)-Cost(price);
}

int main(){
	int price;
	printf("Expected Profit at a given Ticket Price: \n\n");
	for(price=0; price<=50; price+=5)
	{
		printf("Ticket Price = Rs.%d \t\t Profit = Rs.%d\n",price,Profit(price));
		printf("\n\n");
    }
		return 0;
	}




